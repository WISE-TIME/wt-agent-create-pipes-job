def instance = jenkins.model.Jenkins.instance

def agent = "${AGENT_TYPE}"

int discardBuild = System.getenv("DISCARD_BUILD") as Integer ?: 21

pipelineJob("Detect New Tags (${agent})") {

  parameters {
    stringParam('AGENT_TYPE', "${agent}")
  }

  definition {
    cpsScm {
      scm {
        git {
          remote {
            url("https://bitbucket.org/WISE-TIME/wt-upload-tags.git")
          }
          branch("master")
        }
      }
    }
  }

  triggers {
    cron('H/3 * * * *')
  }

  logRotator(discardBuild)

  properties {
    disableConcurrentBuilds()
  }
}

pipelineJob("Posted Time Listener (${agent})") {

  parameters {
    stringParam('AGENT_TYPE', "${agent}")
  }

  definition {
    cpsScm {
      scm {
        git {
          remote {
            url("https://bitbucket.org/WISE-TIME/wt-post-time-job.git")
          }
          branch("master")
        }
      }
    }
  }

  triggers {
    cron('H/3 * * * *')
  }

  logRotator(discardBuild)

  properties {
    disableConcurrentBuilds()
  }
}

pipelineJob("Agent Software Updater (${agent})") {

  parameters {
    stringParam('AGENT_TYPE', "${agent}")
  }

  definition {
    cpsScm {
      scm {
        git {
          remote {
            url("https://bitbucket.org/WISE-TIME/wt-update-agent.git")
          }
          branch("master")
        }
      }
    }
  }

  triggers {
    cron('@daily')
  }

  properties {
    disableConcurrentBuilds()
  }
}

//Start download task
queue("Agent Software Updater (${agent})")

instance.setCrumbIssuer(new hudson.security.csrf.DefaultCrumbIssuer(true))
instance.save()
