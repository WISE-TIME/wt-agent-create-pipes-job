This is repository contains scripts that will create scheduled pipelines for WT ageent integartion (based on Jenkins DSL API).

## Description

Required job param is AGENT_TYPE (e.g. jira, invoice-demo etc). Be sure to set it in env variables (e.g. docker run -e AGENT_TYPE=jira ...)

3 pipelines going to be created by this script:

- **wt-update-agent**.  
  Job to download and install latest wt-agent.

- **wt-post-time-agent**.  
  Job that will integrate WT post time event into third party services (e.g. add worklog time in jira or create report for posted time etc).
  
- **wt-upload-tags**.  
  Job that will get new tags from third party services and upload them to WT.
  
Every job will run daily.

Job **wt-update-agent** will be started upon creation.